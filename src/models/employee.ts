export class employee {
    id: number;
    firstName?: string;
    lastName?: string;
    designation?: string;
    salary?: string;
    shortbio?: string;
    dateofbirth?: Date;
}