import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeListComponents } from './employee/list/employeelist.component';
const routes: Routes = [

  { path: 'employee', component: EmployeeListComponents },
  { path: 'employee', loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule) },

  // { path: 'employee', component: EmployeeListComponents},
  // { path: '**', redirectTo: '/employee' }
  { path: '**', redirectTo: 'employee' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
