import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeDetailsComponent } from './details/details.component'
import { EmployeeListComponents } from './list/employeelist.component'

const routes: Routes = [
     //Employee Routing
     {
      path: 'employee',
      component: EmployeeListComponents},
      {
      path: 'details',
      component: EmployeeDetailsComponent},
      {
      path: 'details/:id',
      component: EmployeeDetailsComponent}
    ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmployeeRoutingModule {}
