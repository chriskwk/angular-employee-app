import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, UrlSerializer } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService } from '../../../services/alert.service';
import { employee } from '../../../models/employee'
@Component({
  selector: 'employee-employeelist',
  styleUrls: ['./details.component.scss'],
  templateUrl: './details.component.html',
})
export class EmployeeDetailsComponent implements OnInit {

  employee : employee;
  form!: FormGroup;
  id!: string;
  isAddMode!: boolean;
  loading = false;
  submitted = false;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private alertService: AlertService
  ) {
    this.employee = new employee();
    this.route.params.subscribe((res) => {
      this.employee.id = res['id']
    });
  }

  ngOnInit() {
      this.id = this.route.snapshot.params['id'];
      this.form = this.formBuilder.group({
          id: [],
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          designation: ['', Validators.required],
          salary: ['', Validators.required],
          shortbio: ['', Validators.required],
          dateofbirth: ['', Validators.required]
      });

      if(this.id){
        this.getDetails();
      }
}

// Provide new ID for User
  getNewUserId(){
    const oldRecords = localStorage.getItem('employeeList');
    if (oldRecords !== null){
      const employeeList = JSON.parse(oldRecords);
      return employeeList.length + 1;
    } else {
      return 1;
    }
  }

  //Submit Form
  onSubmit(){

    this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }
    
    const latestId = this.getNewUserId();
    this.form.value.id = latestId;
    const oldRecords = localStorage.getItem('employeeList');

    if(this.id){
      const oldRecords = localStorage.getItem('employeeList');
      if (oldRecords !== null){
        const employeeList = JSON.parse (oldRecords);
        employeeList.splice(employeeList.findIndex((a:any) => a.id == this.employee.id),1);
        employeeList.push (this.form.value);
        localStorage.setItem('employeeList',JSON.stringify(employeeList));
        this.goToEmployeeList();
      }
    } 
    else {
    if (oldRecords !== null) {
      const employeeList = JSON.parse(oldRecords);
      employeeList.push(this.form.value);
      localStorage.setItem('employeeList', JSON.stringify(employeeList));
      this.goToEmployeeList();
    } else {
      const userArr : any[]=[];
      userArr.push(this.form.value);
      localStorage.setItem('employeeList', JSON.stringify(userArr));
      this.goToEmployeeList();
    }
  }
}

//Patch Form Values on Edit
getDetails(){
  const oldRecords = localStorage.getItem('employeeList');
    if (oldRecords != null){
      const employeeList = JSON.parse(oldRecords);
      const currentUser = employeeList.find(m => m.id == this.employee.id);
      this.form.patchValue({
        firstName: currentUser.firstName,
        lastName: currentUser.lastName,
        salary: currentUser.salary,
        designation: currentUser.designation,
        shortbio: currentUser.shortbio,
        dateofbirth: currentUser.dateofbirth,
      })
    }
}

  get f() 
  { 
    return this.form.controls; 
  }

  //Re-route to Employee Listing Page
  goToEmployeeList(){
    this.router.navigate(['/employee']);
  }

}

