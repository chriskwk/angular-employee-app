import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { EmployeeDetailsComponent } from './details/details.component'
import { EmployeeListComponents } from './list/employeelist.component'

import { EmployeeRoutingModule } from './employee-routing.module';
import { MatTableModule } from '@angular/material/table';

import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap'

import { NgbPaginationModule, NgbTypeaheadModule, NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule} from 'ngx-mask'
@NgModule({
  declarations: [
    EmployeeDetailsComponent,
    EmployeeListComponents
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EmployeeRoutingModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbNavModule,
    NgbPaginationModule,
    NgbCollapseModule,
    NgbTypeaheadModule,
    MatTableModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
  ]
})
export class EmployeeModule { }
