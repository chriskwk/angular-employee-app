import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { employee } from '../../../models';

@Component({
  selector: 'employee-employee',
  styleUrls: ['./employeelist.component.scss'],
  templateUrl: './employeelist.component.html'
})
export class EmployeeListComponents{

  employeeList: employee[];

    constructor
    (
      private router: Router,
      private route: ActivatedRoute,
    ) {
      this.employeeList = [];
    }

    ngOnInit() {
      const records = localStorage.getItem('employeeList');
      if (records !== null) {
        this.employeeList = JSON.parse(records);
      }
      
    }

    AddNew(){
      this.router.navigate(['/employee/details']);
    }

    editClick(e) {
      this.router.navigate(['/employee/details', { id: e }], { relativeTo: this.route });
    }

    //Delete Function
    deleteClick(id:any){
      const oldRecords = localStorage.getItem('employeeList');
      if (oldRecords != null){
        const employeeList = JSON.parse(oldRecords);
        employeeList.splice(employeeList.findIndex((a:any) => a.id == id),1);
        localStorage.setItem('employeeList',JSON.stringify(employeeList));
      }
      const records = localStorage.getItem('employeeList');
      if (records != null){
        this.employeeList = JSON.parse(records);
      }
    }  
}
